package PizzaDecorator;

public class PizzaBocadillo implements PerepararPizza {

    public final PerepararPizza pizza;
    public PizzaBocadillo(PerepararPizza pizza){
        this.pizza = pizza ;
    }
    @Override
    public void construirHamburguesa() {
        this.pizza.construirHamburguesa();
        System.out.println("Agregar Bocadillo");
    }
}
