package PizzaDecorator;

public class PizzaPina implements PerepararPizza {

    public final PerepararPizza pizza;
    public PizzaPina(PerepararPizza pizza){
        this.pizza = pizza ;
    }
    @Override
    public void construirHamburguesa() {
        this.pizza.construirHamburguesa();
        System.out.println("Agregar Piña");
    }
}
