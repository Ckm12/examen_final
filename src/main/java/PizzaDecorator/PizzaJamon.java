package PizzaDecorator;

public class PizzaJamon implements PerepararPizza {

    public final PerepararPizza pizza;
    public PizzaJamon(PerepararPizza pizza){
        this.pizza = pizza ;
    }
    @Override
    public void construirHamburguesa() {
        this.pizza.construirHamburguesa();
        System.out.println("Agregar Jamón");
    }
}
