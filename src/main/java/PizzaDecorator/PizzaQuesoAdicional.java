package PizzaDecorator;

public class PizzaQuesoAdicional implements PerepararPizza {

    public final PerepararPizza pizza;
    public PizzaQuesoAdicional(PerepararPizza pizza){
        this.pizza = pizza ;
    }
    @Override
    public void construirHamburguesa() {
        this.pizza.construirHamburguesa();
        System.out.println("Agregar Queso Adicional ");
    }
}
