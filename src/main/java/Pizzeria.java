import PizzaDecorator.*;
import PizzaSIngleton.java.AnadirPuntos;
import PizzaSIngleton.java.Usuario;

public class Pizzeria {

    public static void main (String[] args) {


        // Aqui se hacen las instancias de los ususarios demostrando el patron singleton



        Usuario usuario1 = new Usuario("Usuario 1");
        Usuario usuario2 = new Usuario("Usuario 2");


        // aqui se declara las pizzerias como pizzeria 1,2,3 suponiendo que son sucursales


        int Pizzeria1 = AnadirPuntos.getInstance().getPuntos();
        usuario1.visitarPizzeria();
        usuario1.visitarPizzeria();
        usuario2.visitarPizzeria();


        int Pizzeria2 = AnadirPuntos.getInstance().getPuntos();

        usuario1.visitarPizzeria();
        usuario2.visitarPizzeria();
        usuario2.visitarPizzeria();


        int Pizzeria3 = AnadirPuntos.getInstance().getPuntos();

        usuario1.visitarPizzeria();
        usuario2.visitarPizzeria();
        usuario2.visitarPizzeria();
        usuario2.visitarPizzeria();
        usuario2.visitarPizzeria();
        usuario1.visitarPizzeria();
        usuario1.visitarPizzeria();


        int puntosUsuario1 = usuario1.getPuntos();
        int puntosUsuario2 = usuario2.getPuntos();

        int puntosUsuarios = puntosUsuario1+puntosUsuario2;

        System.out.println("      Metodo Singleton      ");


        System.out.println("Puntos de la pizzerías " + puntosUsuarios);
        System.out.println("Puntos de " + usuario1.getNombre() + ": " + puntosUsuario1);
        System.out.println("Puntos de " + usuario2.getNombre() + ": " + puntosUsuario2);

        System.out.println("              ");
        System.out.println("              ");
        System.out.println("              ");




        // Aqui se demostrara el patron Decorator
        System.out.println("      Metodo Decorator        ");

        // Suponiendo que ususarios piden diferentes combinaciones


        // Suponiendo que un usuario 2 pidio: pizza base + queso adicional + peperoni
        System.out.println("     Pizza ususario 1         ");
        PizzaPeperoni pizzaUsuario1 = new PizzaPeperoni(new PizzaQuesoAdicional(new PizzaBase()));
        pizzaUsuario1.construirHamburguesa();
        System.out.println("              ");
        System.out.println("              ");

        // Suponiendo que un usuario 2 pidio: pizza base + bocadillo y piña
        System.out.println("     pizza ususario 2        ");
        PizzaPina pizzaUsuario2 = new PizzaPina(new PizzaBocadillo(new PizzaBase()));
        pizzaUsuario2.construirHamburguesa();
        System.out.println("              ");
        System.out.println("              ");



        // Suponiendo que un usuario 3 pidio: pizza base con todos los ingredientes
        System.out.println("      pizza ususario 3        ");
        PizzaPina pizzaUsuario3 = new PizzaPina(new PizzaBocadillo (new PizzaJamon(new PizzaPeperoni(new PizzaQuesoAdicional(new PizzaBase())))));
        pizzaUsuario3.construirHamburguesa();





        /**
        PizzaBase pizzaBase = new PizzaBase();
        PizzaBocadillo pizzaBocadillo = new PizzaBocadillo(pizzaBase);
        PizzaJamon pizzaJamon = new PizzaJamon(pizzaBocadillo);
        PizzaPeperoni pizzaPeperoni = new PizzaPeperoni(pizzaJamon);
        PizzaPina pizzaPina = new PizzaPina(pizzaPeperoni);
        PizzaQuesoAdicional pizzaQuesoAdicional = new PizzaQuesoAdicional(pizzaPina);
        pizzaQuesoAdicional.construirHamburguesa();
         **/
    }




}


//pepino, jamón, piña y bocadillo.