package PizzaSIngleton.java;

public class AnadirPuntos {
    private static AnadirPuntos instance;
    private int puntos;

    private AnadirPuntos() {
        puntos = 0;
    }

    public static AnadirPuntos getInstance() {
        if (instance == null) {
            instance = new AnadirPuntos();
        }
        return instance;
    }

    public void anadirPuntos(int cantidad) {
        puntos += cantidad;
    }

    public int getPuntos() {
        return puntos;
    }
}
