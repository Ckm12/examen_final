package PizzaSIngleton.java;

public class Usuario {
    private String nombre;
    private int puntos;

    public Usuario(String nombre) {
        this.nombre = nombre;
        this.puntos = 0;
    }

    public void visitarPizzeria() {
        AnadirPuntos anadirPuntos = AnadirPuntos.getInstance();
        anadirPuntos.anadirPuntos(1);
        puntos++;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPuntos() {
        return puntos;
    }
}


